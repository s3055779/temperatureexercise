package nl.utwente.di.temperature;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*** Tests temperature calculation */
public class TestTemperature {
    public TemperatureCalculator temperatureCalculator;

    @BeforeEach
    public void setUp() {
        temperatureCalculator = new TemperatureCalculator();
    }

    @Test
    public void testTemperature1() throws Exception {
        double result = temperatureCalculator.getFahrenheitTemperature("25");
        assertEquals(77, result);
    }

    @Test
    public void testTemperature2() throws Exception {
        double result = temperatureCalculator.getFahrenheitTemperature("34");
        assertEquals(93.2, result);
    }

    @Test
    public void testTemperature3() throws Exception {
        double result = temperatureCalculator.getFahrenheitTemperature("14");
        assertEquals(57.2, result);
    }
}

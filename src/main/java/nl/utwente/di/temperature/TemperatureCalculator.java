package nl.utwente.di.temperature;

public class TemperatureCalculator {
    public double getFahrenheitTemperature(String celsiusTemperature) {
        return ((Double.parseDouble(celsiusTemperature)*9)/5)+32;
    }
}
